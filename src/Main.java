import java.util.Scanner;

public class Main {

    private FileManager fm;
    private Logger logger;

    //Constructur with instances of Logger and Filemanager objects.
    public Main(FileManager fm, Logger logger) {
        this.fm = fm;
        this.logger = logger;
    }


    //Helper function to allow back-to-pack searches. With the after search concluded the user
    //has the option of searching another time. If YES entered the function is called recursivly.
    //Calls fileLoggin function from Logger.java to log operation to file.
    //Calls the serachByWord function from FileManager.java to get results.
    public void searchWord(String path){
        Scanner myScanner = new Scanner(System.in);
        System.out.println("Please enter word: ");
        String searchTerm = myScanner.next().toLowerCase();

        long start = System.currentTimeMillis();
        int counter = fm.searchByWord(path,searchTerm);
        long end = System.currentTimeMillis();
        long diff = end-start;

        logger.fileLogging(": The term '" + searchTerm+ "' was found " + counter+ " times. The function took ", diff);
        System.out.println("");
        System.out.println("Do you want to search another word?(YES/NO)");
        String choice4 = myScanner.next();
        if(choice4.equals("YES")) {
            searchWord(path);
        }else if(!choice4.equals("YES") && !choice4.equals("NO")) {
            throw new IllegalArgumentException("Illegal input.");
        }
    }

    //Menu for operations concerning dracula txt file.
    //Asks for input from user to choose between different choices.
    //Calls the appropriate functions from FileManager depending on choices made.
    //Calls the fileLogger in Logger to log the events.
    //recursivly calls itself in the end if user do not opt out and go back to main menu.
    public void draculaSubMenu() {
        String choice3 ="";
        Scanner myScanner = new Scanner(System.in);
        String path = FileManager.getTextFilePath();

        System.out.println("---------------------------------------------------");
        System.out.println("Dracula");
        System.out.println("---------------------------------------------------");
        System.out.println("1. Retrieve filename.");
        System.out.println("2. Retrive filesize. ");
        System.out.println("3. Retrieve number of lines. ");
        System.out.println("4. Search by word. ");
        System.out.println("5. Back to menu");
        Integer draculaChoice = myScanner.nextInt();

        if(draculaChoice == 1) {

            long start = System.currentTimeMillis();
            String name = fm.getFileName(path);
            long end = System.currentTimeMillis();
            long diff = end-start;
            logger.fileLogging(": Filename is " + name + ". The function took "+diff, diff);

        }else if(draculaChoice == 2){
            long start = System.currentTimeMillis();
            long fileSize = fm.getFileSize(path);
            long end = System.currentTimeMillis();
            long diff = end-start;
            logger.fileLogging(": Filesize is " + fileSize + " bytes. The function took "+diff, diff);

        }else if(draculaChoice == 3) {
            long start = System.currentTimeMillis();
            int numOfLines = fm.getNumberOfLines(path);
            long end = System.currentTimeMillis();
            long diff = end-start;
            logger.fileLogging(": The file has " + numOfLines + " lines. The function took "+diff, diff);

        }else if(draculaChoice == 4) {
            System.out.println(" ");
            searchWord(path);

        }else if(draculaChoice == 5) {
            Menu();
        }
        draculaSubMenu();
    }

    //Main menu.
    //Lets the user choose between different tasks depending on userinput from Scanner.
    //Allows the user to exit program entering "NO" when asked if they want to return to menu.
    //recurisvly calls Menu(), if they choose "YES".
    public void Menu(){
        Scanner myScanner = new Scanner(System.in);
        System.out.println("---------------------------------------------------");
        System.out.println("Welcome to FileManager!");
        System.out.println("---------------------------------------------------");
        System.out.println("1. Retrieve all files. ");
        System.out.println("2. Retrieve files by extension. ");
        System.out.println("3. Explore Dracula Script. ");
        System.out.println("0. Exit Program ");
        Integer choice = myScanner.nextInt();

        if(choice == 1){
            System.out.println("All files: ");
            System.out.println("");
            String[] fileNames = fm.listFiles(FileManager.getFilePath());
            for(String name : fileNames) {
                System.out.println(name);
            }
        }else if(choice == 2) {
            System.out.println("Enter Extension. Type one of the following with period .jpeg, .jpg, .png ,.jfif or .txt: ");
            String ext = myScanner.next();
            System.out.println("All files with the extension, " + ext + ": ");
            System.out.println("");
            fm.listByExtension(ext, FileManager.getFilePath());
        }else if(choice == 3){
            draculaSubMenu();
        }else if(choice == 0) {
            System.out.println("Exiting program...");
            System.exit(0);
        }else {
            throw new IllegalArgumentException("Illegal input");
        }

        System.out.println("");
        System.out.println("Do you want to go back to the menu? (YES/NO)");
        String choice2 = myScanner.next();

        if(choice2.equals("YES")){
            Menu();
        }else if(choice2.equals("NO")){
            System.out.println("Exiting program...");
            System.exit(0);
        }else {
            throw new IllegalArgumentException("Illegal input");
        }
    }

    //Initializing program.
    public static void main(String[] args) {
        FileManager fm = new FileManager();
        Logger log = new Logger();
        Main main = new Main(fm,log);
        main.Menu();
    }
}
