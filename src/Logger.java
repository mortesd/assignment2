import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class Logger {

    //Function for logging filemanipulation to file.
    //param message containing information regarding which operation has been done.
    //param diff containgin duration of the operation completed.
    public void fileLogging(String msg, long diff ) {
        String currentTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SS").format(new Date());
        File file = new File("./src/logger/log.txt");

        try{
            FileWriter myWriter = new FileWriter(file,true);
            BufferedWriter bw = new BufferedWriter(myWriter);
            bw.write(currentTime + msg + diff +" milliseconds" + "\n");
            bw.close();
            System.out.println(currentTime + msg + diff + " milliseconds.");
        }catch (IOException e) {
            System.out.println("error");
            e.printStackTrace();
        }
    }
}
