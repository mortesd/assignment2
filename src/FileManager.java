import java.io.*;

public class FileManager {


    private static String filePath = "./recources";
    private static String textFilePath = "./recources/dracula.txt";


    public void FileManager(String filePath) {
        this.filePath = filePath;

    }


    public static String getFilePath() {
        return filePath;
    }
    public static String getTextFilePath() {
        return textFilePath;
    }

    //Boolean method to check if ext is valid.
    public boolean extensionIsValid(String ext){
        String[] validExtensions = {".jpg",".png", ".jpeg", ".jfif", ".txt"};
        for(String extension : validExtensions) {
            if(extension.equals(ext)) {
                return true;
            }
        }
        return false;
    }

    //function to list all files in the recource folder.
    public String[] listFiles(String filePath) {
        String[] fileNames;
        File file = new File(filePath);
        fileNames = file.list();
        return fileNames;
    }

    // function listing files by extension.
    //param ext entered with scanenr object when called.
    //param filePath, path to recource folder.
    //throws exception if ext entered dont pass vaildaton function.
    public void listByExtension(String ext, String filePath) {
        if (!extensionIsValid(ext)) {
            throw new IllegalArgumentException("You have provided an illigal extension");
        } else {
            String[] filenames = listFiles(filePath);
            for (String fileName : filenames) {
                if (fileName.endsWith((ext))) {
                    System.out.println(fileName);
                }
            }
        }
    }

    //function retriving name of file.
    // param filepath
    public String getFileName(String fp) {
        String fileName ="";
        try {
            File file = new File(fp);
            fileName = file.getName();
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return fileName;
    }

    //function retrieving filesize in bytes.
    public long getFileSize(String fp) {
        long fileSize = 0;
        try {
            File file = new File(fp);
            fileSize = file.length();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return fileSize;
    }

    //function retrieving the number of lines textfile has.
    // param filepath
    public int getNumberOfLines(String fp){
        int counter = 0;
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(fp))) {
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                counter++;
            }
        }
        catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        return counter;
    }

    //function letting user search the txt file word terms and how many times they occur in the text.
    //param filepath
    //param term etered by user with Scanner object.
    public int searchByWord(String fp, String term){
        int counter = 0;
        try(BufferedReader br = new BufferedReader(new FileReader(fp));) {
            FileManager fm = new FileManager();
            String line;
            while ((line = br.readLine()) != null) {
                String lineLower = line.toLowerCase();
                String arrayOfWords[] = lineLower.split("\\W+");
                for (String word : arrayOfWords) {
                    if (word.equals(term)) {
                        counter++;
                    }
                }
            }
        }catch(IOException ex) {
            System.out.println(ex.getMessage());
        }
        return counter;
    }
}
