# Filemanager

## Introducion

Filemanager is a console application which lets the user retrieve information from folders and text 
files. In addition the user can search for terms in a textfile and see how many times the term occures. 

## Classes.

The app has three classes: 
* FileManager
* Logger
* Main

##### FileManager

The FileManager class handles all the operations linked with maniulating the texfile "dracula.txt". It lets you retrieve filename, number of lines in the text, filesize, and it lets you search for terms. It also lets the user brows throug the recource folder letting them know what files it contains. 


##### Logger 

The Logger class is responsible for logging events tied with the file maniulating functionality in FileManger. The Logger logs the time of the event, and the duration of the functions execution. The logs are recorded in the log.txt file located in the logger folder. 

##### Main 

Main program. This Class takes both FileManager and Logger and let the user interact with them. The user can enter choices with commands recorded by a scanner object. The class also contains the styling of the app. 


## Visuals. 

Example images. 

##### Menu. 
![menu](images/menu.PNG)

##### Dracula Menu
![dracula menu](images/draculamenu.PNG)

##### Extension search
![ext](images/exExt.PNG)

##### Search by word
![word seach](images/exSearch.PNG)




